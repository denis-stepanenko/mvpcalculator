﻿namespace MVPCalculator.Models
{
    public class Calculator
    {
        public double FirstNumber { get; set; }
        public double SecondNumber { get; set; }

        public double Sum()
        {
            return FirstNumber + SecondNumber;
        }

        public double Substract()
        {
            return FirstNumber - SecondNumber;
        }

        public double Multiply()
        {
            return FirstNumber * SecondNumber;
        }

        public double Divide()
        {
            return FirstNumber / SecondNumber;
        }
    }
}
