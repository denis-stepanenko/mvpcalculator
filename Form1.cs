﻿using MVPCalculator.Presenters;
using MVPCalculator.Views;
using System;
using System.Windows.Forms;

namespace MVPCalculator
{
    public partial class Form1 : Form, ICalculatorView
    {
        CalculatorPresenter _calculatorPresenter;

        public Form1()
        {
            InitializeComponent();
            _calculatorPresenter = new CalculatorPresenter(this);
        }

        public string FirstNumber { get => firstNumberTextBox.Text; set => firstNumberTextBox.Text = value; }
        public string SecondNumber { get => secondNumberTextBox.Text; set => secondNumberTextBox.Text = value; }
        public string Result { get => resultTextBox.Text; set => resultTextBox.Text = value; }

        private void sumButton_Click(object sender, EventArgs e)
        {
            _calculatorPresenter.Sum();
        }

        private void substractButton_Click(object sender, EventArgs e)
        {
            _calculatorPresenter.Substract();
        }

        private void multiplyButton_Click(object sender, EventArgs e)
        {
            _calculatorPresenter.Multiple();
        }

        private void divideButton_Click(object sender, EventArgs e)
        {
            try
            {
                _calculatorPresenter.Divide();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
