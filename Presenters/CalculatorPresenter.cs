﻿using MVPCalculator.Models;
using MVPCalculator.Views;
using System;

namespace MVPCalculator.Presenters
{
    public class CalculatorPresenter
    {
        Calculator _calculator = new Calculator();
        ICalculatorView _calculatorView;

        public CalculatorPresenter(ICalculatorView view)
        {
            _calculatorView = view;
        }

        public void ConnectBetweenModelAndView()
        {
            _calculator.FirstNumber = Convert.ToDouble(_calculatorView.FirstNumber);
            _calculator.SecondNumber = Convert.ToDouble(_calculatorView.SecondNumber);
        }

        public void Sum()
        {
            ConnectBetweenModelAndView();
            _calculatorView.Result = _calculator.Sum().ToString();
        }

        public void Substract()
        {
            ConnectBetweenModelAndView();
            _calculatorView.Result = _calculator.Substract().ToString();
        }

        public void Multiple()
        {
            ConnectBetweenModelAndView();
            _calculatorView.Result = _calculator.Multiply().ToString();
        }

        public void Divide()
        {
            ConnectBetweenModelAndView();

            if (Convert.ToDouble(_calculatorView.SecondNumber) == 0)
            {
                throw new Exception("Can't divide into zero");
            }

            _calculatorView.Result = _calculator.Divide().ToString();
        }
    }
}
