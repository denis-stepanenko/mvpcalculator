﻿namespace MVPCalculator.Views
{
    public interface ICalculatorView
    {
        string FirstNumber { get; set; }
        string SecondNumber { get; set; }
        string Result { get; set; }
    }
}
